package com.alibaba.rocketmq.common.utils;

/**
 *
 */
public class ZKConfig {

	public String getZkConnectionString(){
		return "";
	}

	public int getZkConnectionTimeoutMillis() {
		return 3000;
	}

	public int getZkCloseWaitMillis() {
		return 1000;
	}

	public String getZkNamespace() {
		return "rocketmq";
	}

	public int getSleepMsBetweenRetries() {
		return 100;
	}

	public int getZkRetries() {
		return 3;
	}

	public int getZkSessionTimeoutMillis() {
		return 5 * 1000;
	}
}
